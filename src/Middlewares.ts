import { BasicRequest, ReqResponse, ReqError } from './BasicRequest'
import { Store } from './Store'
import { Extractor } from './Extractor'

export type MiddlewareFunc = (res: ReqResponse, err: ReqError | null, next: Function) => void

/**
 * Return a middleware that
 * execute a request and give result to next middleware
 * @param request : BasicRequest
 * @return Function(res, next)
 */
export function request (request: BasicRequest, store: Store, extractor: Extractor): MiddlewareFunc {
  return async (res, err, next) => {
    /*
      if an error already exists here, it came from a previous step,
      throw it instead of running a new request
     */
    if (err) {
      throw err
    }

    // add headers from storage
    extractor.toHeaders.forEach(data => {
      const value = store.getValue(data.name)
      request.addHeader(data.propName, value)
    })

    // replace url params with value from storage
    extractor.toUrlParams.forEach(data => {
      const value = store.getValue(data.name)
      request.addUrlParam(data.propName, value)
    })

    const { error, response } = await request.run()

    /* if we wants data from response but an error has came
      we better throw it here as it's an unexpected error
    */
    if (error && extractor.fromResponse.length > 0) {
      throw error
    }

    // extract value from response to storage
    extractor.fromResponse.forEach((data) => {
      const value = extractor.getValue(data.propPath, response)
      store.addValue(data.name, value)
    })

    // extract value from error to storage
    extractor.fromError.forEach((data) => {
      const value = extractor.getValue(data.propPath, error)
      store.addValue(data.name, value)
    })

    next(response, error)
  }
}

/**
 * return a middleware that
 * execute a function supposed to contain assertion (like chai)
 * @param fn : Function
 * @return Function(res, next)
 */
export function validate (fn: Function): MiddlewareFunc {
  return (res, err, next) => {
    fn(res, err, next)
  }
}

/**
 * return a middleware that
 * throws an error if response status is not expected value
 * @param fn : Function
 * @return Function(res, next)
 */
export function expectResponseStatusToBe (status: number): MiddlewareFunc {
  return (res, err, next) => {
    const target = status < 400 ? res : err

    if (!target || !target.status) {
      throw err
    } else if (target.status !== status) {
      throw new Error(`Expected status to be ${status}, got ${target.status} instead`)
    }
    next()
  }
}
