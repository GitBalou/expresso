import { BasicRequest, HttpVerbs } from './BasicRequest'
import { Chain } from './Chain'

/**
 * Module expresso
 * Create & chain requests as middlewares
 * Meant to be used for api testing
 * @export
 * @class Expresso
 */
export class Expresso {
  private serverUrl: string
  private chain: Chain

  /**
   * Creates an instance of Expresso
   * @param {string} serverUrl - the tested api url
   * @memberof Expresso
   */
  constructor (serverUrl: string) {
    if (!serverUrl) {
      throw new Error('Expresso: serverUrl should be a non empty string')
    }

    this.serverUrl = serverUrl
  }

  /**
   * Create a post request for target route
   * @param {string} route - Route to test
   * @returns {BasicRequest}
   * @memberof Expresso
   */
  post (route: string) {
    return this.createRequest('post', route)
  }

  /**
   * Create a put request for target route
   * @param {string} route - Route to test
   * @returns {BasicRequest}
   * @memberof Expresso
   */
  put (route: string) {
    return this.createRequest('put', route)
  }

  /**
   * Create a chain and add it to expresso instance
   * @returns {Chain}
   * @memberof Expresso
   */
  createChain () {
    this.chain = new Chain(this.serverUrl)
    return this.chain
  }

  /**
   * Generate a basic request
   * @private
   * @param {HttpVerbs} method - Http verb
   * @param {string} route - Route to test
   * @returns {BasicRequest}
   * @memberof Expresso
   */
  private createRequest (method: HttpVerbs, route: string) {
    return new BasicRequest(this.serverUrl, method, route)
  }
}
