import * as superagent from 'superagent'

/**
 * Supported http methods
 */
export type HttpVerbs = 'get' | 'post' | 'put' | 'delete'

/**
 * Header name & value
 */
interface Headers {
  name: string
  value: number | string | boolean
}

/**
 * Url parameters
 * Matching pattern will be replaced with value
 */
interface UrlParams {
  pattern: string
  value: number | string | boolean
}

/**
 * attached File
 * File from path will be attach to request at named field
 */
interface AttachedFile {
  name: string
  path: string
}

/**
 * Response & error types
 */
export type ReqResponse = superagent.Response
export type ReqError = superagent.ResponseError

/**
 * Basic request
 * Store all needed data for a request and run this request
 * @export
 * @class BasicRequest
 */
export class BasicRequest {
  public title: string
  private route: string
  private method: HttpVerbs
  private body: any
  private serverUrl: string
  private headers: Headers[] = []
  private urlParams: UrlParams[] = []
  private attachedFiles: AttachedFile[] = []

  /**
   * Creates an instance of BasicRequest.
   * @param {string} serverUrl - Tested api url
   * @param {HttpVerbs} method
   * @param {string} route - Target route
   * @memberof BasicRequest
   */
  constructor (serverUrl: string, method: HttpVerbs, route: string) {
    if (!serverUrl) {
      throw new Error('Error creating basic request, serverUrl is undefined')
    }

    if (!route) {
      throw new Error('Error creating basic request, route should not be empty')
    }

    if (['get', 'post', 'delete', 'put'].indexOf(method) === -1) {
      throw new Error('Error creating basic request, method should be one of "get", "post", "delete", "put"')
    }

    this.serverUrl = serverUrl
    this.title = route
    this.route = route
    this.method = method
    this.body = {}
  }

  /**
   * Setter for title
   * @param {string} title
   * @returns {BasicRequest} this - chainable
   */
  setTitle (title: string) {
    if (typeof title !== 'string') {
      throw new Error('BasicRequest: title must be a string')
    }
    this.title = title
    return this
  }

 /**
  * Setter for body
  * @param {any} body
  * @return {basicRequest} this - chainable
  */
  setBody (body: any) {
    this.body = body
    return this
  }

  /**
   * Attach a file to request
   * @param {string} name
   * @param {string} path
   * @return {BasicRequest} this - chainable
   */
  attach (name: string, path: string) {
    if (!name) {
      throw new Error('name is empty')
    }

    if (!path) {
      throw new Error('path is empty')
    }

    this.attachedFiles.push({ name, path })
    return this
  }

 /**
  * Add a header
  * @param name : string
  * @param value : number | string | boolean
  */
  addHeader (name: string, value: number | string | boolean) {
    this.headers.push({ name, value })
  }

 /**
  * Add a url parameter: corresponding pattern in tested route will be
  * replaced by given value
  * @param pattern : string
  * @param value : number | string | boolean
  */
  addUrlParam (pattern: string, value: number | string | boolean) {
    if (this.route.search(pattern) === -1) {
      throw new Error(`Pattern ${pattern} not found in route pattern ${this.route}`)
    }
    this.urlParams.push({ pattern, value })
  }

  /**
   * Run the request
   * Currently using chai-http
   * @returns {Promise.<{response: ReqResponse, error: Error |null>} - Request result : error & response
   * @memberof BasicRequest
   */
  async run (): Promise<{response: ReqResponse, error: ReqError | null}> {
    // set route, replacing url parameters by their values
    const route = this.urlParams.reduce((route, param) => {
      return this.replaceUrlParam(route, param.pattern, param.value)
    }, this.route)

    const request = superagent[this.method](`${this.serverUrl}${route}`)

    // add headers if needed
    this.headers.forEach(header => {
      request.set(header.name, header.value.toString())
    })

    // attach files
    this.attachedFiles.forEach((attachedFile) => {
      request.attach(attachedFile.name, attachedFile.path)
    })

    // add form data
    if (this.body) {
      request.send(this.body)
    }

    // save response & error to return it after validator
    let response: any = {}
    let error = null
    await request
      .then(res => response = res)
      .catch(err => error = err)

    return { error, response }
  }

 /**
  * Clone this instance
  * @returns {BasicRequest} - New instance
  */
  clone () {
    const request = new BasicRequest(this.serverUrl, this.method, this.route)
    request.setTitle(this.title)
    request.setBody(this.body)
    this.attachedFiles.forEach((attachedFile) => {
      request.attach(attachedFile.name, attachedFile.path)
    })
    return request
  }

 /**
  * Replace a given pattern by its value in route url
  * @private
  * @param {string} route
  * @param {string} pattern
  * @param {any} value
  * @returns {string} updated route
  */
  private replaceUrlParam (route: string, pattern: string, value: any) {
    return route.replace(':' + pattern, value)
  }
}
