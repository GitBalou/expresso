import { BasicRequest, ReqResponse, ReqError } from './BasicRequest'
import { Store } from './Store'
import { Extractor, Extracts } from './Extractor'
import * as Middlewares from './Middlewares'

/**
 * Type for failure function
 */
export type Failure = (error: Error) => void

/**
 * Manage chained requests
 */
export class Chain {
  private failure: Function
  private middlewares: Middlewares.MiddlewareFunc[] = []
  private store: Store
  private serverUrl: string

  constructor (serverUrl: string) {
    if (!serverUrl) {
      throw new Error('serverUrl is null')
    }
    this.serverUrl = serverUrl
    this.store = new Store()
  }

  /**
   * Add middleware
   * @param fn Function
   * @return this
   */
  use (fn: Middlewares.MiddlewareFunc) {
    this.middlewares.push(fn)
    return this
  }

  /**
   * Compose & execute middleware chain
   * @param res : response object
   * @param next : middleware (last that will be executed)
   */
  async executeMiddleware (res: ReqResponse | null, err: ReqError| null, next: Function) {
    const composed = this.middlewares.reduceRight((next, fn) => {
      return async (res: ReqResponse, err: ReqError |null) => {
        try {
          await fn(res, err, next)
        } catch (e) {
          this.failure(e)
        }
      }
    }, next)
    await composed()
  }

  /**
   * Launch midddleware chain with a success and a failure callback
   * @param success: Function
   * @param failure: Function
   */
  async end (success: Function, failure: Failure) {
    if (!success || typeof success !== 'function') {
      throw new Error('success should be a function')
    }
    if (!failure || typeof failure !== 'function') {
      throw new Error('failure should be a function')
    }

    // failure callback is called in catch block of running middlewares
    this.failure = failure

    // check if there is really no error at the end of chain before calling sucess
    const realSuccess = (res, err, next) => {
      if (err && Object.keys(err).length > 0) {
        return this.failure(err)
      }
      return success()
    }

    await this.executeMiddleware(null, null, realSuccess)
  }

  /**
   * Launch midddleware chain as a promise
   * @return {Promise<any>}
   */
  async endAsPromise () {
    return new Promise(async (resolve, reject) => {
      // failure callback is called in catch block of running middlewares
      this.failure = reject

      // check if there is really no error at the end of chain before calling sucess
      const realSuccess = (res, err, next) => {
        if (err && Object.keys(err).length > 0) {
          return this.failure(err)
        }
        return resolve()
      }

      await this.executeMiddleware(null, null, realSuccess)
    })
  }

  /**
   * Add a request middleware to the chain
   * @param request : BasicRequest
   * @param extracts: Extracts
   * @return this
   */
  request (request: BasicRequest, extracts: Extracts = {}) {
    const clonedReq = request.clone()
    const extractor = new Extractor(extracts)
    const execRequest = Middlewares.request(clonedReq, this.store, extractor)
    this.use(execRequest)
    return this
  }

  /**
   * Create & add a get request to the chain
   * @param route: string
   * @param extracts: Extracts
   * @param expectedStatus: number
   * @return this
   */
  get (route: string, extracts: Extracts = {}, expectedStatus?: number) {
    const request = new BasicRequest(this.serverUrl, 'get', route)
    const extractor = new Extractor(extracts)
    const execRequest = Middlewares.request(request, this.store, extractor)
    this.use(execRequest)

    if (expectedStatus) {
      this.expectResponseStatusToBe(expectedStatus)
    }

    return this
  }

  /**
   * Create & add a post request to the chain
   * @param route: string
   * @param extracts: Extracts
   * @param expectedStatus: number
   * @return this
   */
  post (route: string, body: any, extracts: Extracts = {}, expectedStatus?: number) {
    const request = new BasicRequest(this.serverUrl, 'post', route)
    request.setBody(body)
    const extractor = new Extractor(extracts)
    const execRequest = Middlewares.request(request, this.store, extractor)
    this.use(execRequest)

    if (expectedStatus) {
      this.expectResponseStatusToBe(expectedStatus)
    }

    return this
  }

  /**
   * Create & add a put request to the chain
   * @param route: string
   * @param extracts: Extracts
   * @param expectedStatus: number
   * @return this
   */
  put (route: string, body: any, extracts: Extracts = {}, expectedStatus?: number) {
    const request = new BasicRequest(this.serverUrl, 'put', route)
    request.setBody(body)
    const extractor = new Extractor(extracts)
    const execRequest = Middlewares.request(request, this.store, extractor)
    this.use(execRequest)

    if (expectedStatus) {
      this.expectResponseStatusToBe(expectedStatus)
    }

    return this
  }

  /**
   * Create & add a delete request to the chain
   * @param route: string
   * @param extracts: Extracts
   * @param expectedStatus: number
   * @return this
   */
  delete (route: string, extracts: Extracts = {}, expectedStatus?: number) {
    const request = new BasicRequest(this.serverUrl, 'delete', route)
    const extractor = new Extractor(extracts)
    const execRequest = Middlewares.request(request, this.store, extractor)
    this.use(execRequest)

    if (expectedStatus) {
      this.expectResponseStatusToBe(expectedStatus)
    }

    return this
  }

  /**
   * Add a validating middleware to the chain
   * @param fn : Middlewares.MiddlewareFunction
   * @return this
   */
  validate (fn: Middlewares.MiddlewareFunc) {
    const execValidate = Middlewares.validate(fn)
    this.use(execValidate)
    return this
  }

  /**
   * Add a expectResponseStatusToBe middleware to the chain
   * @param status: number
   * @return this
   */
  expectResponseStatusToBe (status: number) {
    const execMiddleware = Middlewares.expectResponseStatusToBe(status)
    this.use(execMiddleware)
    return this
  }
}
