/**
 * Extracts interface
 * key is supposed to be res.body.value etc...
 * value is supposed to be a string
 */
export interface Extracts {
  [key: string]: string
}

/**
 * Getter interface
 * store getter extracts like res.body.id : id as
 * - propPath : res.body.id,
 * - name: id
 */
export interface Getter {
  propPath: string
  name: string
}

/**
 * Setter interface
 * store setter extracts like req.header.authorization: token as
 * - propPath: req.header.authorization
 * - propName: authorization
 * - name: token
 */
export interface Setter {
  propPath: string
  name: string
  propName: string
}

/**
 * Extractor, manage extracts string
 * res.body.url <= getter for response.body.url property
 * err.status <= getter for error.status
 * req.params.id <= setter for request url :id pattern
 * req.headers.authorization <= setter for header authorization
 */
export class Extractor {
  public toHeaders: Setter[] = []
  public toUrlParams: Setter[] = []
  public fromResponse: Getter[] = []
  public fromError: Getter[] = []

  constructor (extracts: Extracts) {
    Object.keys(extracts).forEach((key, index) => {
      const props = key.split('.')
      const targetObj = props.shift()

      // sort getters
      if (targetObj === 'res') {
        this.fromResponse.push({ propPath: props.join('.'), name: extracts[key] })
        return
      }

      if (targetObj === 'err') {
        this.fromError.push({ propPath: props.join('.'), name: extracts[key] })
        return
      }

      // sort setters
      const targetProp = props.shift()

      if (props.length === 0) {
        throw new Error(`Missing one nested level in ${key}`)
      }

      if (targetObj === 'req' && targetProp === 'params') {
        this.toUrlParams.push({ propPath: key, name: extracts[key], propName: props[props.length - 1] })
        return
      }

      if (targetObj === 'req' && targetProp === 'headers') {
        this.toHeaders.push({ propPath: key, name: extracts[key], propName: props[props.length - 1] })
        return
      }

      // error if we reach here
      throw new Error(`Invalid extracts propertys: ${key} should be
        one of res.prop or err.prop for getters, or req.params.prop req.headers.prop for setters`)
    })
  }

  /**
   * Extract a value from a nested object
   * @param propPath : string like res.body.id
   * @param source : any response, error, etc..
   * @return number | string | boolean
   */
  getValue (propPath: string, source: any): number | string | boolean {
    const props = propPath.split('.')

    return props.reduce((acc, key) => {
      if (!acc[key]) {
        throw new Error(`Nested props ${key} in ${propPath} not found in source object`)
      }
      acc = acc[key]
      return acc
    }, source)
  }
}
