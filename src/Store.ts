/**
 * Storage interface
 */
interface Storage {
  [key: string]: string | number | boolean
}

/**
 * Storage class
 */
export class Store {
  private storage: Storage = {}

  /**
   * Add a value in storage at specified key
   * @param keyName : string
   * @param value : string | number | boolean
   * @param storage : Storage
   */
  addValue (keyName: string, value: string | number | boolean): void {
    if (this.storage[keyName]) {
      throw new Error(`Key ${keyName} already exists in storage`)
    }
    this.storage[keyName] = value
  }

  /**
   * Return a value from storage at specified key
   * @param keyName : string
   * @param storage : Storage
   * @return string | number | boolean
   */
  getValue (keyName: string): string | number | boolean {
    if (!this.storage[keyName]) {
      throw new Error(`Key ${keyName} does not exists in storage`)
    }
    return this.storage[keyName]
  }
}
