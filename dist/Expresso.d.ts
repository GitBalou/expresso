import { BasicRequest } from './BasicRequest';
import { Chain } from './Chain';
/**
 * Module expresso
 * Create & chain requests as middlewares
 * Meant to be used for api testing
 * @export
 * @class Expresso
 */
export declare class Expresso {
    private serverUrl;
    private chain;
    /**
     * Creates an instance of Expresso
     * @param {string} serverUrl - the tested api url
     * @memberof Expresso
     */
    constructor(serverUrl: string);
    /**
     * Create a post request for target route
     * @param {string} route - Route to test
     * @returns {BasicRequest}
     * @memberof Expresso
     */
    post(route: string): BasicRequest;
    /**
     * Create a put request for target route
     * @param {string} route - Route to test
     * @returns {BasicRequest}
     * @memberof Expresso
     */
    put(route: string): BasicRequest;
    /**
     * Create a chain and add it to expresso instance
     * @returns {Chain}
     * @memberof Expresso
     */
    createChain(): Chain;
    /**
     * Generate a basic request
     * @private
     * @param {HttpVerbs} method - Http verb
     * @param {string} route - Route to test
     * @returns {BasicRequest}
     * @memberof Expresso
     */
    private createRequest(method, route);
}
