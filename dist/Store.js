"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Storage class
 */
var Store = /** @class */ (function () {
    function Store() {
        this.storage = {};
    }
    /**
     * Add a value in storage at specified key
     * @param keyName : string
     * @param value : string | number | boolean
     * @param storage : Storage
     */
    Store.prototype.addValue = function (keyName, value) {
        if (this.storage[keyName]) {
            throw new Error("Key " + keyName + " already exists in storage");
        }
        this.storage[keyName] = value;
    };
    /**
     * Return a value from storage at specified key
     * @param keyName : string
     * @param storage : Storage
     * @return string | number | boolean
     */
    Store.prototype.getValue = function (keyName) {
        if (!this.storage[keyName]) {
            throw new Error("Key " + keyName + " does not exists in storage");
        }
        return this.storage[keyName];
    };
    return Store;
}());
exports.Store = Store;
//# sourceMappingURL=Store.js.map