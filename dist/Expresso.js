"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BasicRequest_1 = require("./BasicRequest");
var Chain_1 = require("./Chain");
/**
 * Module expresso
 * Create & chain requests as middlewares
 * Meant to be used for api testing
 * @export
 * @class Expresso
 */
var Expresso = /** @class */ (function () {
    /**
     * Creates an instance of Expresso
     * @param {string} serverUrl - the tested api url
     * @memberof Expresso
     */
    function Expresso(serverUrl) {
        if (!serverUrl) {
            throw new Error('Expresso: serverUrl should be a non empty string');
        }
        this.serverUrl = serverUrl;
    }
    /**
     * Create a post request for target route
     * @param {string} route - Route to test
     * @returns {BasicRequest}
     * @memberof Expresso
     */
    Expresso.prototype.post = function (route) {
        return this.createRequest('post', route);
    };
    /**
     * Create a put request for target route
     * @param {string} route - Route to test
     * @returns {BasicRequest}
     * @memberof Expresso
     */
    Expresso.prototype.put = function (route) {
        return this.createRequest('put', route);
    };
    /**
     * Create a chain and add it to expresso instance
     * @returns {Chain}
     * @memberof Expresso
     */
    Expresso.prototype.createChain = function () {
        this.chain = new Chain_1.Chain(this.serverUrl);
        return this.chain;
    };
    /**
     * Generate a basic request
     * @private
     * @param {HttpVerbs} method - Http verb
     * @param {string} route - Route to test
     * @returns {BasicRequest}
     * @memberof Expresso
     */
    Expresso.prototype.createRequest = function (method, route) {
        return new BasicRequest_1.BasicRequest(this.serverUrl, method, route);
    };
    return Expresso;
}());
exports.Expresso = Expresso;
//# sourceMappingURL=Expresso.js.map