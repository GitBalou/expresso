"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var BasicRequest_1 = require("./BasicRequest");
var Store_1 = require("./Store");
var Extractor_1 = require("./Extractor");
var Middlewares = require("./Middlewares");
/**
 * Manage chained requests
 */
var Chain = /** @class */ (function () {
    function Chain(serverUrl) {
        this.middlewares = [];
        if (!serverUrl) {
            throw new Error('serverUrl is null');
        }
        this.serverUrl = serverUrl;
        this.store = new Store_1.Store();
    }
    /**
     * Add middleware
     * @param fn Function
     * @return this
     */
    Chain.prototype.use = function (fn) {
        this.middlewares.push(fn);
        return this;
    };
    /**
     * Compose & execute middleware chain
     * @param res : response object
     * @param next : middleware (last that will be executed)
     */
    Chain.prototype.executeMiddleware = function (res, err, next) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var composed;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        composed = this.middlewares.reduceRight(function (next, fn) {
                            return function (res, err) { return __awaiter(_this, void 0, void 0, function () {
                                var e_1;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            _a.trys.push([0, 2, , 3]);
                                            return [4 /*yield*/, fn(res, err, next)];
                                        case 1:
                                            _a.sent();
                                            return [3 /*break*/, 3];
                                        case 2:
                                            e_1 = _a.sent();
                                            this.failure(e_1);
                                            return [3 /*break*/, 3];
                                        case 3: return [2 /*return*/];
                                    }
                                });
                            }); };
                        }, next);
                        return [4 /*yield*/, composed()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Launch midddleware chain with a success and a failure callback
     * @param success: Function
     * @param failure: Function
     */
    Chain.prototype.end = function (success, failure) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var realSuccess;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!success || typeof success !== 'function') {
                            throw new Error('success should be a function');
                        }
                        if (!failure || typeof failure !== 'function') {
                            throw new Error('failure should be a function');
                        }
                        // failure callback is called in catch block of running middlewares
                        this.failure = failure;
                        realSuccess = function (res, err, next) {
                            if (err && Object.keys(err).length > 0) {
                                return _this.failure(err);
                            }
                            return success();
                        };
                        return [4 /*yield*/, this.executeMiddleware(null, null, realSuccess)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Launch midddleware chain as a promise
     * @return {Promise<any>}
     */
    Chain.prototype.endAsPromise = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var _this = this;
                        var realSuccess;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    // failure callback is called in catch block of running middlewares
                                    this.failure = reject;
                                    realSuccess = function (res, err, next) {
                                        if (err && Object.keys(err).length > 0) {
                                            return _this.failure(err);
                                        }
                                        return resolve();
                                    };
                                    return [4 /*yield*/, this.executeMiddleware(null, null, realSuccess)];
                                case 1:
                                    _a.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    /**
     * Add a request middleware to the chain
     * @param request : BasicRequest
     * @param extracts: Extracts
     * @return this
     */
    Chain.prototype.request = function (request, extracts) {
        if (extracts === void 0) { extracts = {}; }
        var clonedReq = request.clone();
        var extractor = new Extractor_1.Extractor(extracts);
        var execRequest = Middlewares.request(clonedReq, this.store, extractor);
        this.use(execRequest);
        return this;
    };
    /**
     * Create & add a get request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    Chain.prototype.get = function (route, extracts, expectedStatus) {
        if (extracts === void 0) { extracts = {}; }
        var request = new BasicRequest_1.BasicRequest(this.serverUrl, 'get', route);
        var extractor = new Extractor_1.Extractor(extracts);
        var execRequest = Middlewares.request(request, this.store, extractor);
        this.use(execRequest);
        if (expectedStatus) {
            this.expectResponseStatusToBe(expectedStatus);
        }
        return this;
    };
    /**
     * Create & add a post request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    Chain.prototype.post = function (route, body, extracts, expectedStatus) {
        if (extracts === void 0) { extracts = {}; }
        var request = new BasicRequest_1.BasicRequest(this.serverUrl, 'post', route);
        request.setBody(body);
        var extractor = new Extractor_1.Extractor(extracts);
        var execRequest = Middlewares.request(request, this.store, extractor);
        this.use(execRequest);
        if (expectedStatus) {
            this.expectResponseStatusToBe(expectedStatus);
        }
        return this;
    };
    /**
     * Create & add a put request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    Chain.prototype.put = function (route, body, extracts, expectedStatus) {
        if (extracts === void 0) { extracts = {}; }
        var request = new BasicRequest_1.BasicRequest(this.serverUrl, 'put', route);
        request.setBody(body);
        var extractor = new Extractor_1.Extractor(extracts);
        var execRequest = Middlewares.request(request, this.store, extractor);
        this.use(execRequest);
        if (expectedStatus) {
            this.expectResponseStatusToBe(expectedStatus);
        }
        return this;
    };
    /**
     * Create & add a delete request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    Chain.prototype.delete = function (route, extracts, expectedStatus) {
        if (extracts === void 0) { extracts = {}; }
        var request = new BasicRequest_1.BasicRequest(this.serverUrl, 'delete', route);
        var extractor = new Extractor_1.Extractor(extracts);
        var execRequest = Middlewares.request(request, this.store, extractor);
        this.use(execRequest);
        if (expectedStatus) {
            this.expectResponseStatusToBe(expectedStatus);
        }
        return this;
    };
    /**
     * Add a validating middleware to the chain
     * @param fn : Middlewares.MiddlewareFunction
     * @return this
     */
    Chain.prototype.validate = function (fn) {
        var execValidate = Middlewares.validate(fn);
        this.use(execValidate);
        return this;
    };
    /**
     * Add a expectResponseStatusToBe middleware to the chain
     * @param status: number
     * @return this
     */
    Chain.prototype.expectResponseStatusToBe = function (status) {
        var execMiddleware = Middlewares.expectResponseStatusToBe(status);
        this.use(execMiddleware);
        return this;
    };
    return Chain;
}());
exports.Chain = Chain;
//# sourceMappingURL=Chain.js.map