import { BasicRequest, ReqResponse, ReqError } from './BasicRequest';
import { Extracts } from './Extractor';
import * as Middlewares from './Middlewares';
/**
 * Type for failure function
 */
export declare type Failure = (error: Error) => void;
/**
 * Manage chained requests
 */
export declare class Chain {
    private failure;
    private middlewares;
    private store;
    private serverUrl;
    constructor(serverUrl: string);
    /**
     * Add middleware
     * @param fn Function
     * @return this
     */
    use(fn: Middlewares.MiddlewareFunc): this;
    /**
     * Compose & execute middleware chain
     * @param res : response object
     * @param next : middleware (last that will be executed)
     */
    executeMiddleware(res: ReqResponse | null, err: ReqError | null, next: Function): Promise<void>;
    /**
     * Launch midddleware chain with a success and a failure callback
     * @param success: Function
     * @param failure: Function
     */
    end(success: Function, failure: Failure): Promise<void>;
    /**
     * Launch midddleware chain as a promise
     * @return {Promise<any>}
     */
    endAsPromise(): Promise<{}>;
    /**
     * Add a request middleware to the chain
     * @param request : BasicRequest
     * @param extracts: Extracts
     * @return this
     */
    request(request: BasicRequest, extracts?: Extracts): this;
    /**
     * Create & add a get request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    get(route: string, extracts?: Extracts, expectedStatus?: number): this;
    /**
     * Create & add a post request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    post(route: string, body: any, extracts?: Extracts, expectedStatus?: number): this;
    /**
     * Create & add a put request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    put(route: string, body: any, extracts?: Extracts, expectedStatus?: number): this;
    /**
     * Create & add a delete request to the chain
     * @param route: string
     * @param extracts: Extracts
     * @param expectedStatus: number
     * @return this
     */
    delete(route: string, extracts?: Extracts, expectedStatus?: number): this;
    /**
     * Add a validating middleware to the chain
     * @param fn : Middlewares.MiddlewareFunction
     * @return this
     */
    validate(fn: Middlewares.MiddlewareFunc): this;
    /**
     * Add a expectResponseStatusToBe middleware to the chain
     * @param status: number
     * @return this
     */
    expectResponseStatusToBe(status: number): this;
}
