"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var superagent = require("superagent");
/**
 * Basic request
 * Store all needed data for a request and run this request
 * @export
 * @class BasicRequest
 */
var BasicRequest = /** @class */ (function () {
    /**
     * Creates an instance of BasicRequest.
     * @param {string} serverUrl - Tested api url
     * @param {HttpVerbs} method
     * @param {string} route - Target route
     * @memberof BasicRequest
     */
    function BasicRequest(serverUrl, method, route) {
        this.headers = [];
        this.urlParams = [];
        this.attachedFiles = [];
        if (!serverUrl) {
            throw new Error('Error creating basic request, serverUrl is undefined');
        }
        if (!route) {
            throw new Error('Error creating basic request, route should not be empty');
        }
        if (['get', 'post', 'delete', 'put'].indexOf(method) === -1) {
            throw new Error('Error creating basic request, method should be one of "get", "post", "delete", "put"');
        }
        this.serverUrl = serverUrl;
        this.title = route;
        this.route = route;
        this.method = method;
        this.body = {};
    }
    /**
     * Setter for title
     * @param {string} title
     * @returns {BasicRequest} this - chainable
     */
    BasicRequest.prototype.setTitle = function (title) {
        if (typeof title !== 'string') {
            throw new Error('BasicRequest: title must be a string');
        }
        this.title = title;
        return this;
    };
    /**
     * Setter for body
     * @param {any} body
     * @return {basicRequest} this - chainable
     */
    BasicRequest.prototype.setBody = function (body) {
        this.body = body;
        return this;
    };
    /**
     * Attach a file to request
     * @param {string} name
     * @param {string} path
     * @return {BasicRequest} this - chainable
     */
    BasicRequest.prototype.attach = function (name, path) {
        if (!name) {
            throw new Error('name is empty');
        }
        if (!path) {
            throw new Error('path is empty');
        }
        this.attachedFiles.push({ name: name, path: path });
        return this;
    };
    /**
     * Add a header
     * @param name : string
     * @param value : number | string | boolean
     */
    BasicRequest.prototype.addHeader = function (name, value) {
        this.headers.push({ name: name, value: value });
    };
    /**
     * Add a url parameter: corresponding pattern in tested route will be
     * replaced by given value
     * @param pattern : string
     * @param value : number | string | boolean
     */
    BasicRequest.prototype.addUrlParam = function (pattern, value) {
        if (this.route.search(pattern) === -1) {
            throw new Error("Pattern " + pattern + " not found in route pattern " + this.route);
        }
        this.urlParams.push({ pattern: pattern, value: value });
    };
    /**
     * Run the request
     * Currently using chai-http
     * @returns {Promise.<{response: ReqResponse, error: Error |null>} - Request result : error & response
     * @memberof BasicRequest
     */
    BasicRequest.prototype.run = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var route, request, response, error;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        route = this.urlParams.reduce(function (route, param) {
                            return _this.replaceUrlParam(route, param.pattern, param.value);
                        }, this.route);
                        request = superagent[this.method]("" + this.serverUrl + route);
                        // add headers if needed
                        this.headers.forEach(function (header) {
                            request.set(header.name, header.value.toString());
                        });
                        // attach files
                        this.attachedFiles.forEach(function (attachedFile) {
                            request.attach(attachedFile.name, attachedFile.path);
                        });
                        // add form data
                        if (this.body) {
                            request.send(this.body);
                        }
                        response = {};
                        error = null;
                        return [4 /*yield*/, request
                                .then(function (res) { return response = res; })
                                .catch(function (err) { return error = err; })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, { error: error, response: response }];
                }
            });
        });
    };
    /**
     * Clone this instance
     * @returns {BasicRequest} - New instance
     */
    BasicRequest.prototype.clone = function () {
        var request = new BasicRequest(this.serverUrl, this.method, this.route);
        request.setTitle(this.title);
        request.setBody(this.body);
        this.attachedFiles.forEach(function (attachedFile) {
            request.attach(attachedFile.name, attachedFile.path);
        });
        return request;
    };
    /**
     * Replace a given pattern by its value in route url
     * @private
     * @param {string} route
     * @param {string} pattern
     * @param {any} value
     * @returns {string} updated route
     */
    BasicRequest.prototype.replaceUrlParam = function (route, pattern, value) {
        return route.replace(':' + pattern, value);
    };
    return BasicRequest;
}());
exports.BasicRequest = BasicRequest;
//# sourceMappingURL=BasicRequest.js.map