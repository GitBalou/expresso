/// <reference types="superagent" />
import * as superagent from 'superagent';
/**
 * Supported http methods
 */
export declare type HttpVerbs = 'get' | 'post' | 'put' | 'delete';
/**
 * Response & error types
 */
export declare type ReqResponse = superagent.Response;
export declare type ReqError = superagent.ResponseError;
/**
 * Basic request
 * Store all needed data for a request and run this request
 * @export
 * @class BasicRequest
 */
export declare class BasicRequest {
    title: string;
    private route;
    private method;
    private body;
    private serverUrl;
    private headers;
    private urlParams;
    private attachedFiles;
    /**
     * Creates an instance of BasicRequest.
     * @param {string} serverUrl - Tested api url
     * @param {HttpVerbs} method
     * @param {string} route - Target route
     * @memberof BasicRequest
     */
    constructor(serverUrl: string, method: HttpVerbs, route: string);
    /**
     * Setter for title
     * @param {string} title
     * @returns {BasicRequest} this - chainable
     */
    setTitle(title: string): this;
    /**
     * Setter for body
     * @param {any} body
     * @return {basicRequest} this - chainable
     */
    setBody(body: any): this;
    /**
     * Attach a file to request
     * @param {string} name
     * @param {string} path
     * @return {BasicRequest} this - chainable
     */
    attach(name: string, path: string): this;
    /**
     * Add a header
     * @param name : string
     * @param value : number | string | boolean
     */
    addHeader(name: string, value: number | string | boolean): void;
    /**
     * Add a url parameter: corresponding pattern in tested route will be
     * replaced by given value
     * @param pattern : string
     * @param value : number | string | boolean
     */
    addUrlParam(pattern: string, value: number | string | boolean): void;
    /**
     * Run the request
     * Currently using chai-http
     * @returns {Promise.<{response: ReqResponse, error: Error |null>} - Request result : error & response
     * @memberof BasicRequest
     */
    run(): Promise<{
        response: ReqResponse;
        error: ReqError | null;
    }>;
    /**
     * Clone this instance
     * @returns {BasicRequest} - New instance
     */
    clone(): BasicRequest;
    /**
     * Replace a given pattern by its value in route url
     * @private
     * @param {string} route
     * @param {string} pattern
     * @param {any} value
     * @returns {string} updated route
     */
    private replaceUrlParam(route, pattern, value);
}
