"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Return a middleware that
 * execute a request and give result to next middleware
 * @param request : BasicRequest
 * @return Function(res, next)
 */
function request(request, store, extractor) {
    var _this = this;
    return function (res, err, next) { return __awaiter(_this, void 0, void 0, function () {
        var _a, error, response;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    /*
                      if an error already exists here, it came from a previous step,
                      throw it instead of running a new request
                     */
                    if (err) {
                        throw err;
                    }
                    // add headers from storage
                    extractor.toHeaders.forEach(function (data) {
                        var value = store.getValue(data.name);
                        request.addHeader(data.propName, value);
                    });
                    // replace url params with value from storage
                    extractor.toUrlParams.forEach(function (data) {
                        var value = store.getValue(data.name);
                        request.addUrlParam(data.propName, value);
                    });
                    return [4 /*yield*/, request.run()
                        /* if we wants data from response but an error has came
                          we better throw it here as it's an unexpected error
                        */
                    ];
                case 1:
                    _a = _b.sent(), error = _a.error, response = _a.response;
                    /* if we wants data from response but an error has came
                      we better throw it here as it's an unexpected error
                    */
                    if (error && extractor.fromResponse.length > 0) {
                        throw error;
                    }
                    // extract value from response to storage
                    extractor.fromResponse.forEach(function (data) {
                        var value = extractor.getValue(data.propPath, response);
                        store.addValue(data.name, value);
                    });
                    // extract value from error to storage
                    extractor.fromError.forEach(function (data) {
                        var value = extractor.getValue(data.propPath, error);
                        store.addValue(data.name, value);
                    });
                    next(response, error);
                    return [2 /*return*/];
            }
        });
    }); };
}
exports.request = request;
/**
 * return a middleware that
 * execute a function supposed to contain assertion (like chai)
 * @param fn : Function
 * @return Function(res, next)
 */
function validate(fn) {
    return function (res, err, next) {
        fn(res, err, next);
    };
}
exports.validate = validate;
/**
 * return a middleware that
 * throws an error if response status is not expected value
 * @param fn : Function
 * @return Function(res, next)
 */
function expectResponseStatusToBe(status) {
    return function (res, err, next) {
        var target = status < 400 ? res : err;
        if (!target || !target.status) {
            throw err;
        }
        else if (target.status !== status) {
            throw new Error("Expected status to be " + status + ", got " + target.status + " instead");
        }
        next();
    };
}
exports.expectResponseStatusToBe = expectResponseStatusToBe;
//# sourceMappingURL=Middlewares.js.map