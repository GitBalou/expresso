/**
 * Extracts interface
 * key is supposed to be res.body.value etc...
 * value is supposed to be a string
 */
export interface Extracts {
    [key: string]: string;
}
/**
 * Getter interface
 * store getter extracts like res.body.id : id as
 * - propPath : res.body.id,
 * - name: id
 */
export interface Getter {
    propPath: string;
    name: string;
}
/**
 * Setter interface
 * store setter extracts like req.header.authorization: token as
 * - propPath: req.header.authorization
 * - propName: authorization
 * - name: token
 */
export interface Setter {
    propPath: string;
    name: string;
    propName: string;
}
/**
 * Extractor, manage extracts string
 * res.body.url <= getter for response.body.url property
 * err.status <= getter for error.status
 * req.params.id <= setter for request url :id pattern
 * req.headers.authorization <= setter for header authorization
 */
export declare class Extractor {
    toHeaders: Setter[];
    toUrlParams: Setter[];
    fromResponse: Getter[];
    fromError: Getter[];
    constructor(extracts: Extracts);
    /**
     * Extract a value from a nested object
     * @param propPath : string like res.body.id
     * @param source : any response, error, etc..
     * @return number | string | boolean
     */
    getValue(propPath: string, source: any): number | string | boolean;
}
