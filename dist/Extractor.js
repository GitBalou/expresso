"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Extractor, manage extracts string
 * res.body.url <= getter for response.body.url property
 * err.status <= getter for error.status
 * req.params.id <= setter for request url :id pattern
 * req.headers.authorization <= setter for header authorization
 */
var Extractor = /** @class */ (function () {
    function Extractor(extracts) {
        var _this = this;
        this.toHeaders = [];
        this.toUrlParams = [];
        this.fromResponse = [];
        this.fromError = [];
        Object.keys(extracts).forEach(function (key, index) {
            var props = key.split('.');
            var targetObj = props.shift();
            // sort getters
            if (targetObj === 'res') {
                _this.fromResponse.push({ propPath: props.join('.'), name: extracts[key] });
                return;
            }
            if (targetObj === 'err') {
                _this.fromError.push({ propPath: props.join('.'), name: extracts[key] });
                return;
            }
            // sort setters
            var targetProp = props.shift();
            if (props.length === 0) {
                throw new Error("Missing one nested level in " + key);
            }
            if (targetObj === 'req' && targetProp === 'params') {
                _this.toUrlParams.push({ propPath: key, name: extracts[key], propName: props[props.length - 1] });
                return;
            }
            if (targetObj === 'req' && targetProp === 'headers') {
                _this.toHeaders.push({ propPath: key, name: extracts[key], propName: props[props.length - 1] });
                return;
            }
            // error if we reach here
            throw new Error("Invalid extracts propertys: " + key + " should be\n        one of res.prop or err.prop for getters, or req.params.prop req.headers.prop for setters");
        });
    }
    /**
     * Extract a value from a nested object
     * @param propPath : string like res.body.id
     * @param source : any response, error, etc..
     * @return number | string | boolean
     */
    Extractor.prototype.getValue = function (propPath, source) {
        var props = propPath.split('.');
        return props.reduce(function (acc, key) {
            if (!acc[key]) {
                throw new Error("Nested props " + key + " in " + propPath + " not found in source object");
            }
            acc = acc[key];
            return acc;
        }, source);
    };
    return Extractor;
}());
exports.Extractor = Extractor;
//# sourceMappingURL=Extractor.js.map