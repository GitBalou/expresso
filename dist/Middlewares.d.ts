import { BasicRequest, ReqResponse, ReqError } from './BasicRequest';
import { Store } from './Store';
import { Extractor } from './Extractor';
export declare type MiddlewareFunc = (res: ReqResponse, err: ReqError | null, next: Function) => void;
/**
 * Return a middleware that
 * execute a request and give result to next middleware
 * @param request : BasicRequest
 * @return Function(res, next)
 */
export declare function request(request: BasicRequest, store: Store, extractor: Extractor): MiddlewareFunc;
/**
 * return a middleware that
 * execute a function supposed to contain assertion (like chai)
 * @param fn : Function
 * @return Function(res, next)
 */
export declare function validate(fn: Function): MiddlewareFunc;
/**
 * return a middleware that
 * throws an error if response status is not expected value
 * @param fn : Function
 * @return Function(res, next)
 */
export declare function expectResponseStatusToBe(status: number): MiddlewareFunc;
