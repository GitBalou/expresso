/**
 * Storage class
 */
export declare class Store {
    private storage;
    /**
     * Add a value in storage at specified key
     * @param keyName : string
     * @param value : string | number | boolean
     * @param storage : Storage
     */
    addValue(keyName: string, value: string | number | boolean): void;
    /**
     * Return a value from storage at specified key
     * @param keyName : string
     * @param storage : Storage
     * @return string | number | boolean
     */
    getValue(keyName: string): string | number | boolean;
}
