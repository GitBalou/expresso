# Expresso

Accelerate routes testing for REST api.
Based on Mocha & Chai-http

## Install
* Yarn: `yarn add --dev git@gitlab.com:GitBalou/expresso.git` 
* NPM: `npm install --save-dev git@gitlab.com:GitBalou/expresso.git` 

## Usage with typescript & mocha
```javascript
import Expresso from 'expresso';
const chai = require('chai');

// new test suite
const expresso = new Expresso('http://localhost:3000');

// Add a chain with a post and a get request
describe('Basic testing', () => {

    it('first test', (done) => {
        expresso.chain('My new chain')
            .post(
                '/users', 
                { email: 'myemail@mail.com', name : 'foo' },
                { 'res.body.user.id': 'id' })
            .expectResponseStatusToBe(201)
            .get('/user/:id', { 'req.params.id' : 'id' })
            .validate((res, err, next) => {
                // exemple with chai
                res.body.user.name.should.equal('foo'); 

                // do not forget to call next()
                next();
            })
            .end(() => done(), (err) => done(err)); // Dont forget this middleware to give done as chain is async
    })
})
```

## Usage with typescript & ava
```javascript
import test from 'ava'
import Expresso from 'expresso'

// new test suite
const expresso = new Expresso('http://localhost:3000')

// test
test('API home', t => {
    return expresso.createChain()
    .get('/')
    .expectResponseStatusToBe(200)
    .endAsPromise()
})
```

## Todo
Refactor promise ending